# BobaMod

Simple Minecraft fabric mod that adds a Boba Drink Item that is crafted similarly as Golden Apple except with a Milk Bucket.
It applies the same effect as Golden Apple when consumed.

Run `gradlew build` to build the mod. The built jar file will be inside `build/libs/`

Special thanks to Soul_OoO for the Boba clipart.
