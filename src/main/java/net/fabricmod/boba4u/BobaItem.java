package net.fabricmod.boba4u;

import net.fabricmod.boba4u.registry.ModSounds;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.*;
import net.minecraft.world.World;

import java.util.List;

public class BobaItem extends Item {

    // Set the characteristics of the food item.
    public static final FoodComponent BOBA_COMP = (new FoodComponent.Builder())
            .alwaysEdible() //remove this later
            .hunger(4)
            .saturationModifier(9.6f)
            .statusEffect(new StatusEffectInstance(StatusEffects.ABSORPTION,2400,0),1f) //  2 mins
            .statusEffect(new StatusEffectInstance(StatusEffects.REGENERATION,100,1),1f) // 5 sec
            .build();

    public static final Settings BOBA_SETTINGS = new Settings()
            .group(BobaMod.BOBA_GROUP)
            .food(BobaItem.BOBA_COMP)
            .rarity(Rarity.RARE);

    public static final Identifier BOBA_DRANK = new Identifier(
            BobaMod.MOD_ID, "boba_drank");

    public BobaItem(Settings settings) {
        super(settings);
    }

    @Override
    public UseAction getUseAction(ItemStack stack)
    {
        return UseAction.DRINK;
    }

    @Override
    public SoundEvent getEatSound() {
        return ModSounds.BOBA_DRINK_SOUND_EVENT;
    }

    @Override
    public SoundEvent getDrinkSound() {
        return SoundEvents.ENTITY_GENERIC_DRINK;
    }

    @Override
    public ItemStack finishUsing(ItemStack stack, World world, LivingEntity user)
    {
        //todo? Can down casting be avoided ?
        if (user instanceof PlayerEntity player) {
            player.incrementStat(BOBA_DRANK);
        }
        return super.finishUsing(stack,world,user);
    }

    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        // formatted red text
        tooltip.add( new TranslatableText("item.boba4u.boba.tooltip").formatted(Formatting.DARK_RED) );
    }


}
