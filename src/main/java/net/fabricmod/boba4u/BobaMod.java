package net.fabricmod.boba4u;

// Modded items registry

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.fabricmod.boba4u.registry.ModItems;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

public class BobaMod implements ModInitializer {

    // global MOD_ID for import
    public static final String MOD_ID = "boba4u";

    // Create MOD GROUP
    public static final ItemGroup BOBA_GROUP = FabricItemGroupBuilder.build(

            new Identifier(MOD_ID,"general"),
            () -> new ItemStack(ModItems.BOBA_ITEM)
    );

    @Override
    public void onInitialize() {
        ModItems.registerSounds();
        ModItems.registerItems();
        ModItems.registerStats();
    }
}
