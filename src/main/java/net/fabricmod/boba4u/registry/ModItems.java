package net.fabricmod.boba4u.registry;

import net.fabricmod.boba4u.BobaItem;
import net.fabricmod.boba4u.BobaMod;
import net.minecraft.sound.SoundEvent;
import net.minecraft.stat.StatFormatter;
import net.minecraft.stat.Stats;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import net.fabricmod.boba4u.registry.ModSounds;

public class ModItems {

    /*********
    BOBA related
    **********/

    // Create the Boba as item.
    public static final BobaItem BOBA_ITEM = new BobaItem(BobaItem.BOBA_SETTINGS);

    // Register the items
    public static void registerItems()
    {
        Registry.register(Registry.ITEM,new Identifier(BobaMod.MOD_ID, "boba"), BOBA_ITEM);
    }

    public static void registerStats()
    {
        Registry.register(Registry.CUSTOM_STAT, "boba_drank", BobaItem.BOBA_DRANK);
        Stats.CUSTOM.getOrCreateStat(BobaItem.BOBA_DRANK, StatFormatter.DEFAULT);
    }
    public static void registerSounds()
    {
        Registry.register(Registry.SOUND_EVENT, ModSounds.BOBA_DRINK_SOUND_EVENT.getId(), ModSounds.BOBA_DRINK_SOUND_EVENT);
    }
}
