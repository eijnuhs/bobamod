package net.fabricmod.boba4u.registry;

import net.fabricmod.boba4u.BobaMod;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;


public class ModSounds {
    private static final String BOBA_SOUND_ID_STR = "boba.drink";
    public static final SoundEvent BOBA_DRINK_SOUND_EVENT = createSoundEvent(BOBA_SOUND_ID_STR);

    public static SoundEvent createSoundEvent(String soundIDStr) {
        Identifier soundID = new Identifier(BobaMod.MOD_ID,soundIDStr);
        return new SoundEvent(soundID);
    }
}
